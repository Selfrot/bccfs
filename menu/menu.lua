_G.BCCFSMenu = _G.BCCFSMenu or {}
BCCFSMenu._path = ModPath .. "menu/"
BCCFSMenu._data_path = SavePath .. "bccfs_saved_data.txt"

BCCFSMenu._data = {
    toggle_chat_message_value = true,
    toggle_show_waypoint_value = true
}

BCCFSMenu._data.RH = BCCFSMenu._data.RH or 0
BCCFSMenu._data.GS = BCCFSMenu._data.GS or 0.75
BCCFSMenu._data.BV = BCCFSMenu._data.BV or 0

function BCCFSMenu:_get_color(rh, gs, bv)
    return Color(1, rh, gs, bv)
end

function BCCFSMenu:Save()
    local file = io.open(self._data_path, "w+")
    if file then
        file:write(json.encode(self._data))
        file:close()
    end
end

function BCCFSMenu:Load()
    local file = io.open(self._data_path, "r")
    if file then
        self._data = json.decode(file:read("*all"))
        file:close()
    end
end

Hooks:Add("LocalizationManagerPostInit", "LocalizationManagerPostInit_BCCFSMenu", function(loc)
    local current_language = nil
    local supported_languages = {
        ["english"] = "en",
        ["german"] = "de",
        ["french"] = "fr",
        ["italian"] = "it",
        ["spanish"] = "es",
        ["russian"] = "ru",
        ["dutch"] = "nl",
        ["swedish"] = "sv"
    }
    for k,v in pairs(supported_languages) do
        if Idstring(k):key() == SystemInfo:language():key() then
            log("[BCCFS] Current language is: " .. v)
            current_language = v
        end
    end

    if current_language then -- Check if language got detected properly
        local _path = BCCFSMenu._path .. current_language .. ".txt"

        if io.file_is_readable(_path) then -- Check if file exists
            loc:load_localization_file(BCCFSMenu._path .. current_language .. ".txt")
            loc:load_localization_file(BCCFSMenu._path .. "en.txt", false) -- Any missing strings in the lang file will be substituted with english ones
        else -- If the file doesn't exist, load the english one
            loc:load_localization_file(BCCFSMenu._path .. "en.txt")
        end
    end

--    local language = Idstring("russian"):key() == SystemInfo:language():key() and "ru" or "en"
--    loc:load_localization_file(BCCFSMenu._path .. language .. ".txt")
--
--    loc:load_localization_file(BCCFSMenu._path .. "en.txt", false)
end)

function BCCFSMenu:ShowPreviewMenuItem()
      if not managers.menu_component then
          return
      end

      local ws = managers.menu_component._ws
      self._panel = ws:panel():panel()

      local w, h = self._panel:w() * 0.35, 48
      self._color_rect = self._panel:rect({
            w = w,
            h = h,
            color = Color.red,
            blend_mode = "add",
            layer = tweak_data.gui.MOUSE_LAYER - 50,
      })
      self._color_rect:set_right( self._panel:right() )
      self._color_rect:set_top( self._panel:h() * 0.265 )

      BCCFSMenu:Save()
      self:UpdatePreview()
end

function BCCFSMenu:DestroyPreviewMenuItem()
      if alive(self._panel) then

            self._panel:remove( self._color_rect )
            self._panel:remove( self._panel )

            self._color_rect = nil
            self._panel = nil

            BCCFSMenu:Save()
      end
end

function BCCFSMenu:UpdatePreview()
      if not alive(self._panel) or not alive(self._color_rect) then
            return
      end

      self._color_rect:set_color( BCCFSMenu:_get_color(BCCFSMenu._data.RH, BCCFSMenu._data.GS, BCCFSMenu._data.BV) )
end

Hooks:Add("MenuManagerInitialize", "MenuManagerInitialize_BCCFSMenu", function(menu_manager)

      MenuCallbackHandler.BCCFSMenuChangeFocus = function(node, focus)
            if focus then
                  BCCFSMenu:ShowPreviewMenuItem()
            else
                  BCCFSMenu:DestroyPreviewMenuItem()
            end
      end

      MenuCallbackHandler.BCCFSMenuSetRedHue = function( this, item )
            BCCFSMenu._data.RH = tonumber( item:value() )
            BCCFSMenu:UpdatePreview()
      end

      MenuCallbackHandler.BCCFSMenuSetGreenSaturation = function( this, item )
            BCCFSMenu._data.GS = tonumber( item:value() )
            BCCFSMenu:UpdatePreview()
      end

      MenuCallbackHandler.BCCFSMenuSetBlueValue = function( this, item )
            BCCFSMenu._data.BV = tonumber( item:value() )
            BCCFSMenu:UpdatePreview()
      end

      MenuCallbackHandler.callback_toggle_chat_message = function(self, item)
            BCCFSMenu._data.toggle_chat_message_value = item:value() == "on" and true or false
            BCCFSMenu:Save()
      end

      MenuCallbackHandler.callback_toggle_show_waypoint = function(self, item)
            BCCFSMenu._data.toggle_show_waypoint_value = item:value() == "on" and true or false
            BCCFSMenu:Save()
      end

    BCCFSMenu:Load()

    MenuHelper:LoadFromJsonFile(BCCFSMenu._path .. "menu_options.txt", BCCFSMenu, BCCFSMenu._data)
end)
